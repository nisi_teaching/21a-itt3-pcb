---
Week: 48
Content:  PCB rev2 in Kicad
Material: See links in weekly plan
Initials: NISI
---

# Week 48 - PCB rev2 in Kicad

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* project log updated
* PCB revision 2 created in Kicad, including manufacturing files 

### Learning goals

* Kicad
    * Level 1: The student can transfer, PCB design and production knowledge, from Orcad to Kicad
    * Level 2: The student can use online ressources to learn how to use Kicad
    * Level 3: The student can create a 2nd revision PCB revision in Kicad


## Schedule

* 08:15 Exercises without teacher (K2/K3)
* 12:15 Exercises with teacher (K1/K4)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww47](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww47)

## Comments

* none