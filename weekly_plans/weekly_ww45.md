---
Week: 45
Content:  Routing, gerber and drill files, ordering PCB's
Material: See links in weekly plan
Initials: NISI
---

# Week 45 - Routing, gerber and drill files, ordering PCB's

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* PCB routed
* Errors cleared using DRC
* PCB ordered from JLCPCB
* Project log updated on gitlab

### Learning goals
* PCB routing
    * Level 1: The student can route a PCB using an autorouter
    * Level 2: The student can route a PCB taking manufacturer capabilities into consideration
    * Level 3: The student can route a PCB taking best practices and manufacturer capabilities into consideration

* PCB manufacturing
    * Level 1: The student can prepare manufacturing files from instructions
    * Level 2: The student can ensure manufacturing files quality using inspection tools
    * Level 3: The student can produce and ensure quality manufacturing files + order PCB's according to a set deadline

## Schedule

* 08:15 Exercises without teacher (K2/K3)
* 12:15 Theory lectures (K1)
* 13:15 Exercises with teacher (K4)
* 14:45 Exercises without teacher (K2/K3)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww45](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww45)

## Comments

* none