---
Week: 37
Content:  Design rule check, netlist, Board outline
Material: See links in weekly plan
Initials: NISI
---

# Week 37 - Design rule check, netlist

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Read and Complete all exercises
* Project log updated
* Design rule check performed
* Netlist created
* Board outline created
* Mounting holes placed on board
* Catch up on exercises from previous weeks

### Learning goals
* Design rule check, netlist
    * Level 1: The student knows the purpose of a netlist
    * Level 2: The student can perform a design rule check and correct errors
    * Level 3: The student can create and export a netlist to pcb designer

* Board outline + mechanical symbols
    * Level 1: The student knows what a board outline is
    * Level 2: The student can set up a PCB design parameters 
    * Level 3: The student can create a board outline and mechanical symbols


## Schedule

* 08:15 Exercises without teacher (K2/K3)
* 13:00 Exercises with teacher (K1/K4)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww37](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww37)


## Comments