---
Week: 39
Content: Workshop - Design for manufacturing
Material: See links in weekly plan
Initials: NISI
---

# Week 39 - Design for manufacturing workshop

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Participate in Design for manufacturing workshop
* Update project log
* Catch up on exercises from previous weeks

### Learning goals
* Design for manufacturing (LGK2, LGK3, LGS1, LGS2, LGC3, LGC5)
    * Level 1: The student has basic knoledge of design for manufacturing
    * Level 2: The student can identify design for manufacturing needs from the workshop in their PCB project
    * Level 3: The student can apply design for manufaturing considerations in their PCB project


## Schedule

* 08:15 Exercises without teacher (K2/K3)
* 13:00 Design for manufacturing workshop, Søren Elsner Knudsen - Mandatory (Universal Robots) (K1)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww39](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww39)

## Comments

* This week is with visit by Søren from Universal Robots who is doing a special Design for manufacturing workshop for us, attendance is mandatory 