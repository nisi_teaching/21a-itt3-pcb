---
Week: 47
Content:  PCB Assembly, PCB test
Material: See links in weekly plan
Initials: NISI
---

# Week 47 - PCB Assembly and test

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Update project log
* Assemble and test PCB

### Learning goals

* Assembly and test
    * Level 1: The student can assemble a PCB
    * Level 2: The student can assemble and test a PCB
    * Level 3: The student can assemble, test and produce test documentation

## Schedule

* 08:15 Exercises with teacher (K4)
* 10:45 Exercises without teacher (K2/K3)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww47](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww47)

## Comments

* none