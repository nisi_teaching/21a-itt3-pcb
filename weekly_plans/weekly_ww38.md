---
Week: 38
Content: Workshop - Nordcad
Material: See links in weekly plan
Initials: NISI
---

# Week 38 - Nordcad workshop

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Prepare question for the Nordcad workshop
* Participate in Nordcad workshop
* Update project log
* Catch up on exercises from previous weeks

### Learning goals
* Nordcad workshop (LGS1, LGS4, LGC2, LGC5)
    * Level 1: The student can prepare project related questions 
    * Level 2: The student can communicate technical challenges
    * Level 3: The student can implement new knowledge in to the PCB project


## Schedule

* 08:15 Exercises without teacher (K2/K3)
* 13:00 Nordcad workshop hosted by Nordcad - Mandatory (K1/K4)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww38](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww38)


## Comments

* This week is with visit by Martin from Nordcad who is doing a special Orcad workshop for us, attendance is mandatory 