---
Week: 49
Content:  Exam report
Material: See links in weekly plan
Initials: NISI
---

# Week 49 - Report, exam hand-in

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Exam report created and handed in on wiseflow

### Learning goals
* Exam report
    * Level 1: The student can write a report 
    * Level 2: The student can write a report that meets the requirements
    * Level 3: The student can write a report that meets the requirements as well as the guidelines


## Schedule

* 08:15 Exercises without teacher (K2/K3)
* 12:15 Exercises with teacher (K1/K4)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww49](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww49)

## Comments

* none