---
Week: 46
Content:  Documentation (BOM, schematics, other design files)
Material: See links in weekly plan
Initials: NISI
---

# Week 46 - Documentation (BOM, schematics, other design files)

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Project log updated
* Gitlab project updated

### Learning goals
* Documentation
    * Level 1: The student knows what relevant PCB design documentation is
    * Level 2: The student can produce relevant PCB design documentation
    * Level 3: The student can present relevant PCB design documentation in a useful way

## Deliverables

* PCB design documentation created
* Exercises todo list included in gitlab readme.md

## Schedule

* 08:15 Exercises without teacher (K2/K3)
* 12:15 Exercises with teacher (K1/K4)
* 14:45 Exercises without teacher (K2/K3)
* 15:30 End of day

## Hands-on time (K2/K3/K4)

See the exercise document for detailed information about exercises:  
[https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww46](https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww46)

## Comments

* none