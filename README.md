![Build Status](https://gitlab.com/EAL-ITT/21a-itt3-pcb/badges/master/pipeline.svg)


# 21A-ITT3-PCB

weekly plans, resources and other relevant stuff for courses.

public website for students:

*  [https://eal-itt.gitlab.io/21a-itt3-pcb/](https://eal-itt.gitlab.io/21a-itt3-pcb/)

students gitlab group:

* [https://gitlab.com/21a-itt3-pcb-students-group](https://gitlab.com/21a-itt3-pcb-students-group)