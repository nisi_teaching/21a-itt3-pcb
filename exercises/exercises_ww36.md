---
Week: 36
tags:
- SMT packages
- Footprints
- Component research
---


# Exercises for ww36

### Exercise 1 - SMT component packages

### Information

The benefit of using smt components in your design is smaller component sizes, fewer standard packages and the possibility to do automated PCB assembly on pick and place machines.
Some of the drawbacks are that they can be very small and some of the packages does not have exposed leads for hand soldering. 

The purpose of this exercise is for you to gain knowledge and understanding of the different packages that exists and the different sizes. 

Ressources for research:
* [https://www.electronics-notes.com/articles/electronic_components/surface-mount-technology-smd-smt/packages.php](https://www.electronics-notes.com/articles/electronic_components/surface-mount-technology-smd-smt/packages.php)
* [https://www.pcbonline.com/blog/pcb-smt-components.html](https://www.pcbonline.com/blog/pcb-smt-components.html)
* [https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types#PIN-PITCH](https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types#PIN-PITCH)
* [https://en.wikipedia.org/wiki/Surface-mount_technology](https://en.wikipedia.org/wiki/Surface-mount_technology)

### Exercise instructions

1. Create a spreadsheet to note your findings
2. Research *Passive rectangular components* and, in your document, note their package type/name, an image, dimensions in mm and dimensions in inches. 
3. Research *Transistor & diode packages* and, in your document, note their package type/name,, an image, dimensions in mm and dimensions in inches. 
4. Research *Integrated circuit SMD packages* and, in your document, note their package type/name, an image, pin configuration, pin spacing in mm and inches.
5. In the component room next to E-Lab try to find as many of the different smd component types to get an impression of their sizes.  
This will also help you to know what we already have in stock at UCL.  

Please do leave the components in their location ie. do not remove them form their location in the component room.  
6. Include your spreadsheet as a pdf in your gitlab project.

## Exercise 2 - Component research 

### Information 

From your schematic and circuit research you should be able to create a list of the components you need for your PCB.

### Exercise instructions

1. Make a spreadsheet to note your findings, suggested columns are: Component name, package type/name, designator, supplier, price.
2. Gather datasheets for all chosen components and include them in the gitlab project.
3. Include the spreadsheet in your gitlab project.

## Exercise 3 - Footprints

### Information 

Before your can transfer your circuit from capture to pcb designer you need to assign footprints to all of your components.
Many footprints are available online, especially standard smd packages but in rare cases you might need to create your own footprints.  

This assignment uses a number of videos to instruct you in assigning footprints and prepare your circuit for pcb editor.

Before starting this exercise, create a copy of the OrCad project and rename it to indicate this is the PCB layout version of the schematic.   
The process of changing a simulation schematic to a schematic with footprints might inable you to run the simulations again.

### Exercise instructions

Watch the video:  
OrCAD 17.2 PCB Design Tutorial - 04 - Capture: Preparing for Manufacture[https://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=4](https://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=4)

You can also use video 2-8 as reference for this proces, link to videos: [https://resources.orcad.com/orcad-capture-tutorials](https://resources.orcad.com/orcad-capture-tutorials)

1. Gather footprints for your circuit (see ressources in comments)  Instructions [https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=6](https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=6) 
2. Examine the footprints to make sure they meet the specs in the datasheet.  You might need to change the footprint? [https://youtu.be/s8mYUTix3ao](https://youtu.be/s8mYUTix3ao)
3. If you can't find a footprint online you will have to make it yourself - video 8 - 14 in this playlist has instructions [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx) 
4. Update your component spreadsheet with a footprint link for each component

## Comments

Footprint suppliers:
Ultralibrarian [https://www.ultralibrarian.com/](https://www.ultralibrarian.com/)  
Componentsearchengine [http://componentsearchengine.com/](http://componentsearchengine.com/)  
Octopart [https://octopart.com/](https://octopart.com/)  
Others: TI, Mouser, Farnell etc.

