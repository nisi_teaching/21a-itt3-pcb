---
Week: 45
tags:
- Routing
- Fabrication files
- PCB order
---


# Exercises for ww45

## Exercise 1 - Route the board

### Information

When routing your PCB it is important that you take current into consideration when desiding on trace width.

Some best practices in these articles:

1. Ten-best-practices-of-PCB-design [https://www.edn.com/electronics-blogs/all-aboard-/4429390/Ten-best-practices-of-PCB-design](https://www.edn.com/electronics-blogs/all-aboard-/4429390/Ten-best-practices-of-PCB-design)  
2. pcb-design-guide [https://www.pannam.com/blog/pcb-design-guide/](https://www.pannam.com/blog/pcb-design-guide/)  

Other best practices:

* When routing ensure that you do not have trace angles steeper than 45 degrees, if you have steeper angles acid from the manufacturing proces can lead to removing to much copper i these areas.

* Use copper pour and connect it to ground. Use copper pour on both sides of your pcb. This also means that when routing you do not connect ground nets with wires, they will connect automatically when you create the ground connected copper pour.  
This is especially important if you do larger PCB's. They can bend if you do not have a sensible copper balance on both sides of your PCB.

* Make sure that your traces are wide enough to carry the needed current. Make sure to stay well above the manufacturers (JLCPCB) capabilities.
A trace width of 0.3mm for signals and 0.5mm for power is a good rule of thumb.
To be sure you can use a trace width calculator [https://www.4pcb.com/trace-width-calculator.html](https://www.4pcb.com/trace-width-calculator.html)

* It is not reccommended to use the autorouter in Orcad as it does not give good results, manual routing gives the best results. 

### Exercise instructions

1. Watch Video 19 - 20 [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)
2. Route your pcb, start with the power (VCC) net and continue with the signal nets. Minimize traces on the bottom side of the board.
Remember to end traces to through hole components on the bottom layer and on the top layer for SMT components.  


## Exercise 2 - Silkscreen

## Information

The silkscreen layer holds the text on your PCB. The purpose of the silkscreen is to provide information about the board. The needed information is the designators for each component (C1, R2, J2 etc.) in order to make assembly and troubleshooting as easy as possible when looking at the bill of material and schematic.

### Exercise instructions

1. Add or edit component designators to make sure that it is easy to assemble and use the board.   
2. Add text that shows the name and revision of the board to be able to distinguish them if you make more revisions in the future.  
3. Add extra text to inputs and outputs with the purpose of making it easier to attach it to the outside world.  

Use Video 20 for information [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)

## Exercise 3 - Clear DRC errors

### Information

Design rule check (DRC) is important to do during the entire design phase but especially before ordering the PCB's.

### Exercise instructions

Make sure that you have no DRC errors in your design.  
If you have, resolve them by either moving traces or components.

## Exercise 4 - Design sanity check

### Information

I always perform a sanity check at this stage by printing my PCB on a piece of paper.  
This enables me to check if the components actually fit the design.  

### Exercise instructions

1. Print the design on paper, make sure you have no scaling in the printout, use a 1:1 scale.
2. Place all components on the printout to see if they fit and that you are able to solder them.

## Exercise 5 - Create Gerber and Drill files

## Information

Gerber files holds the manufacturing information for each design layer.   
Drill files holds the manufacturing information that the manufacturer uses when drilling holes in your PCB.  

* Info on the Gerber file format is here: [https://www.ucamco.com/en/gerber](https://www.ucamco.com/en/gerber) (make sure to click the tabs)   
* Info on drill file formats [https://en.wikipedia.org/wiki/PCB_NC_formats](https://en.wikipedia.org/wiki/PCB_NC_formats)

### Exercise instructions

1. Use the menu nordcad->Output and postprocessing->Run post process to create a zip file that contains both your Gerber and drill files.  
You will probably be prompted to place a drill legend above your layout during the proces.

If you do not have the Nordcad menu use the instructions in Video 21 [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)  


## Exercise 6 - Inspect gerbers and drill file

### Information

To make sure that you have all your layers and drill files it is important to inspect them in an external program like gerbv [https://sourceforge.net/projects/gerbv/](https://sourceforge.net/projects/gerbv/) (free) or gerbview[https://www.gerbview.com/](https://www.gerbview.com/) (paid)  


### Exercise instructions

1. Install gerbv
2. Inspect your gerber and drill files, video 22 [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx) gives examples on how to do this.

## Exercise 7 - Order from JLC PCB

### Information

It is possible to order different designs in one order. This will save on parcel cost and therefore it might be a good idea to team up with other students when ordering.

One thing to know when ordering anything from china is that it is near impossible during the [chinese new year](https://chinesenewyear.net/), which takes place in January and February.  
The reason for this is that manufacturing workers visit familiy, usually a long way from where they work.

What the current supply chain status means for the delivery of your OCB is unknown, this is a good reason to make sure that you order your pcb's today!


### Exercise instructions

1. Put your gerber and drill files into a .zip library
1. Create an account at JLCPCB [https://jlcpcb.com/](https://jlcpcb.com/) 
2. Upload the zip containing your gerbers and drill files.
3. Pick delivery by UPS upon checkout, if you dont you will have to wait 3+ weeks. With UPS you should expect it within a week or less.



