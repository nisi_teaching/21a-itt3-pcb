---
Week: 47
tags:
- PCB Test
- PCB Assembly
---


# Exercises for ww47

## Exercise 1 - Assemble produced PCB


### Information 

Hopefully you received your PCB's and parts by now and are ready to assemble your PCB ?

### Exercise instructions

* Assemble your PCB according to the documentation you produced in week 39 (schematic, component placement, Bill of materials)
* Take pictures of the assembly proces for use in the poster exam

### Exercise 2 - Test assembled PCB's

### Information 

In week 37 you did simulations of your subcircuits in Orcad. 
The idea of this exercise is to make repeat the simulations on your physical PCB.

If you did not do simulations in week 37, you probably still have to do some meaningful measurements/tests on your PCB to make sure it is performing as expected. 

Meaningful testing suggestion:

* Functionality tests (is everything working ?)
* Electrical tests (total current, VCC noise, Ground noise ?)
* Signal tests (noise on signal lines, voltage levels correct ?)
* Connectivity tests (test that every connection is working)
* Programming test (can you use your PCb with a programing running on the attached microprocessor ?)
* Thermal performance (is anything getting too hot)


### Exercise instructions

1. Make a list of the tests you are going to perform including expected results.
1. Test your assembled PCB according to the simulation you specified in week 37 and/or relevant tests from the above suggested.
3. Document the test results in a .md file on gitlab.  

Please use test equipment like multimeter and oscilloscope to document your electrical/signal/connectivity tests.  
Use screen shots for software tests and regular photos of other tests (HW setup etc.)

*Note* It is important that you document your test in a uniform manner, that is using the same format for decimal results, screenshots of simulations etc. 




