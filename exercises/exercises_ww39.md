---
Week: 39
tags:
- Design for manufacturing workshop
---


# Exercises for ww39

### Exercise 1 - Questions

### Information 

Todays we have a workshop hosted Søren Elsner, the topic is design for manufacturing.  
This means that you have a chance to ask questions to an expert in PCB manufacturing!  

To get the most of it you need to find out what questions you would like to ask!

### Exercise instructions

1. Read about design for manufacturing [https://www.mclpcb.com/blog/design-for-manufacturing-pcbs/](https://www.mclpcb.com/blog/design-for-manufacturing-pcbs/)
1. Prepare questions about manufacturing before the workshop - examine these links to get inspiration for questions [https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/links](https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/links)
2. Coordinate with other students at the PCB elective, if you have similar question combine them.
3. Ask your questions at the workshop

### Exercise 1 - Design for manufacturing workshop

### Information 

Todays exercise is a workshop hosted by Søren Elsner - [NPI engineer](https://bestjobdescriptions.com/manufacturing/npi-engineer-job-description) at Universal Robots.
Particiation is mandatory

### Exercise instructions

2. Make sure that you are in room B2.05/B2.49 well before 13:00, the workshop starts at 13:00
3. Follow the instructions in the workshop and take notes during the workshop
4. Update your worklog! 

```
