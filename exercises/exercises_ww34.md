---
Week: 34
tags:
- Introduction
- Project setup
- Worklog
- PCB process
- Circuit decision
- Circuit research
---


# Exercises for ww34

## Exercise 1 - Circuit research

### Information

The goal of the PCB course is to develop transform circuits into professional PCB's. 
If you look at the learning goals described in the lecture plan [https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/21A_ITT3_PCB_lecture_plan.html](https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/21A_ITT3_PCB_lecture_plan.html) you can see that many of them is about embedded systems. 
Because of this, a requirement for the circuit you choose is to include either a microprocessor on the circuit or that the circuit is an add-on to a microprocessor system (hat, carrier board etc.)
You might already have a circuit that you want to develop into a pcb, if so let me know what the idea and circuit is and i will see if it can be approved according to the learning goals.  
It is also okay to chose a design that you will use in the ITT3 project or one of the other elective courses, as long as it fits the learning goals of the PCB elective course.  
If you do not have a circuit in mind, you can pick one from the circuit catalogue [https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/circuit_catalogue.html](https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/circuit_catalogue.html)


### Exercise instructions

1. Research and decide on the circuit you want to design and build
2. Create a block diagram with logical subcircuits and specifications for input and output values between subcircuits.  
3. Document with a schematic drawn in Orcad capture.
4. If needed, identify and document test points and their values. 

If you do not have a circuit chosen by week 35 (next week) I will ask you to work with one of the circuits from the circuit catalogue.

## Exercise 2 - Setup gitlab documentation project

### Information 

Your work in the PCB course needs to be documented in a gitlab project, it will be part of the exam assesment at the end of the course.  

### Exercise instructions

Setup a gitlab project in the gitlab group [https://gitlab.com/21a-itt3-pcb-students-group](https://gitlab.com/21a-itt3-pcb-students-group) and use the naming **ITT3-PCB-YOURNAME** for the project  

The group is private so ask nisi to include you in the group.  

## Exercise 3 - Project log

### Information 

The PCB course is focused around the process of making pcb's, to keep track of the process you need to create a log where you log your experience about the PCB creation process.    
This is mandatory and will be valuable when you have to prepare a pitch for the exam and later in your career.  

### Exercise instructions

1. In your gitlab project create a file called `worklog.md`  

    The log shall, as a minimum, for each working day include:  

    * Summary of activities (What did you plan to do and what did you actually do)
    * Lessons learned (compared to the learning goals)
    * Ressources used (Links, lectures etc.)

## Exercise 4 - OrCad installation and license

### Information 

You need to have the OrCad suite installed in order to develop a PCB.  
You also need a working OrCad student license to access all the needed functionality compared to the free OrCad version.  
You can get a student license during your entire education. However, a license is only valid for one year at the time.  
If you have both OrCad installed and a working student license you do not need to complete this exercise.   

### Exercise instructions

1. Apply or renew your student license from [https://www.nordcad.eu/student-forum/get-student-licens/](https://www.nordcad.eu/student-forum/get-student-licens/)
2. Follow further instructions received in your mail to download and install the OrCad suite.
3. Use the trial version until you receive your license