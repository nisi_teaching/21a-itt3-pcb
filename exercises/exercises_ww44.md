---
Week: 44
tags:
- Design constraints
- Layers
- Board layout
- Copper plane
---


# Exercises for ww44

### Exercise 1 - Board outline

### Information

The board outline is the physical outline of your PCB, often it is in a square shape but you can do any shape that fits your project.

### Exercise instructions

Create an outline for your PCB. The outline is the size and shape of your PCB.

* Use Video 16-17 from this playlist [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)  

* Use tutorial 03 from Cadence [https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-03-set-up-pcb-design-and-draw-a-board-outline](https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-03-set-up-pcb-design-and-draw-a-board-outline)


## Exercise 2 - Mounting holes using mechanical symbols

### Information 

Mounting holes or other mechanical holes are often placed on a PCB. 
In this exercise you will learn how to create and place mechanical symbols on your PCB

### Exercise instructions

* Use tutorial 04 from Cadence [https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols](https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols)


## Exercise 3 - Component placement

### Information 

In this exercise you will learn how to place components on your PCB

### Exercise instructions

* Use tutorial 05 from Cadence [https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-05-place-components](https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-05-place-components)


## Exercise 4.1 - Design Guidelines for Successful Manufacturing 

### Information 

In week 39 we had a workshop about design for manufacturing.  
There was a lot of information and you might not remember all of it.  
Luckily there are a few good resources to recap what you need to consider when designing for manufacturing.

### Exercise instructions

1. Read the Design for manufacturing guidelines from Altium [https://www.altium.com/design-manufacturing-resources](https://www.altium.com/design-manufacturing-resources)
2. Read the Design for manufacturing guidelines from Cadence [https://resources.pcb.cadence.com/blog/design-for-manufacturing-or-dfm-analysis-pcb-dfm-process-slp](https://resources.pcb.cadence.com/blog/design-for-manufacturing-or-dfm-analysis-pcb-dfm-process-slp)
3. In your worklog.md, write a list of DFM considerations that you are going to implement in your PCB Design


### Exercise 4.2 - Setup constraints in Orcad PCB editor

Constraints are a set of rules that ensures that you do nor exceed the capabilities of your PCB manufacturer.  
The constraints is used to guide you when you layout your PCB and is the set of rules that the design rule check (DRC) follows.  

If you need it you can recap the basics of the constraint manager from the Nordcad workshop material [https://www.nordcad.dk/filer/KomNemtIGangmedOrCAD.zip](https://www.nordcad.dk/filer/KomNemtIGangmedOrCAD.zip) the file **4_PCB_Design_using_OrCAD.pdf** has this information

### Exercise instructions

1. Go to [https://jlcpcb.com/capabilities/Capabilities](https://jlcpcb.com/capabilities/Capabilities) and inspect what their capabilities are
2. Use tutorial 06 [https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-06-set-up-differential-pairs-and-constraint-manager](https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-06-set-up-differential-pairs-and-constraint-manager) to setup constraints in the constraint manager according to JLCPCB's capabilities.  

## Exercise 5 - Copper plane and shapes

### Information 

It is normal to fill unrouted space, on both the top and bottom etch layers of your PCB, with a copper plane connected to the ground net.  
Using a ground copper plane has several benefits. 
1. All ground pins on your components will automatically connect to the ground plane removing the need to route seperate ground traces.  
2. Having app. the same amout of copper on both sides of your PCB will "balance the copper" which reduces the risk of your PCB to bend during the manufacturing process
3. It is easy to gain access to establish a ground connection everywhere on your PCB.

When you start routing your PCB next week your routes will push the copper plane and make space on both side of your trace, the space between trace and copper plane is determined by your design constraints.

*Note: In designs with more than 2 layers it is normal to create planes for power as well as for ground* 

This exercise shows you how to use the shape tool in PCB editor and how to assign a net to a copper plane.  

### Exercise instructions

1. Follow tutorial 07 from Cadence [https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-07-create-copper-plane-and-shapes](https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-07-create-copper-plane-and-shapes)
2. Create a copper plane connected to ground on both the top and bottom etch layer of your PCB.