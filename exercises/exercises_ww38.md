---
Week: 38
tags:
- Nordcad workshop
---


# Exercises for ww38

### Exercise 1 - Questions

### Information 

Todays we have a workshop hosted by Nordcad.  
This means that you have a chance to ask questions to an expert in Orcad!  

To get the most of it you need to find out what questions you would like to ask!

### Exercise instructions

1. From your worklog and experience write a list of things you are struggling with in either Capture or PCB editor
2. Coordinate with other students at the PCB elective, if you have similar question combine them.
3. Ask your questions at the workshop

### Exercise 2 - Nordcad workshop

### Information 

Nordcad workshop.
Particiation is mandatory

### Exercise instructions

2. Make sure that you are in room B2.05/B2.49 well before 13:00, the workshop starts at 13:00
3. Follow the instructions at the workshop and take notes during the workshop
4. Update your worklog! 

