---
Week: 48
tags:
- PCB rev2 in Kicad
---


# Exercises for ww48

### Exercise 1 - PCB revision 2 in KiCad

## Information

Orcad is a very capable suite of programs and a industry standard, but it is also very expensive to purchase. If you work in a company without expensive Orcad licenses there are other free options that can be used. One of these options is Kicad. 

As an exercise in using KiCad, redraw your design in KiCad and include the changes gathered in exercise 0.  

Note the pro's and con's about both KiCad and Orcad in your gitlab log.

If you for some reason do not have to do a revision 2 (no errors in revision 1....) of your PCB, you should do a simple design in KiCad to get experience with the program suite.  
An idea is to do the same PCB but using another microntroller.

## Exercise instructions

1. Install KiCad from [www.kicad-pcb.org](http://www.kicad-pcb.org/)  

2. According to the test results from week 47, create a 2nd revision of your PCB i Kicad including manufacturing and documentation files (gerber, drill, BOM, layout files).  
Instructions and other help at KiCad turorials and documentation [https://docs.kicad.org/5.1/en/getting_started_in_kicad/getting_started_in_kicad.html](https://docs.kicad.org/5.1/en/getting_started_in_kicad/getting_started_in_kicad.html)  
Other KiCad documentation [https://docs.kicad.org/](https://docs.kicad.org/)

3. Include revision 2 files in your gitlab project


## Comments
 
Based on the knowledge you know about the PCB design and manufacturing proces in Orcad, it should not be hard to pick up on KiCad using the supplied tutorials and documentation.

